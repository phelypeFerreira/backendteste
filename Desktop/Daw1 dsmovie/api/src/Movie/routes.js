module.exports = (app) => {
  const auth = require("../core/auth.js");
  const controller = require("./controller.js");

  //Busca todos os filmes
  app.get("/movies", controller.findAll);

  //Busca um filme por ID
  app.get("/movies/:id", controller.findById);

  //Criar um novo filme
  app.post("/movies", controller.create);

  //Atualiza um filme por ID
  app.put("/movies", controller.update);

  //Remove um filme por ID
  app.delete("/movies", controller.delete);
};
