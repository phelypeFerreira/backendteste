const db = require("../configs/sequelize.js");
const sequelize = db.sequelize;
const { Model, DataTypes } = db.Sequelize;

class Movie extends Model {}

Movie.init(
  {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    score: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    totalScore: {
      type: DataTypes.DOUBLE,
      allowNull: true,
    },
    count: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    image: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  { sequelize, modelName: "tb_movie", freezeTableName: true, timestamps: false }
);

module.exports = Movie;
