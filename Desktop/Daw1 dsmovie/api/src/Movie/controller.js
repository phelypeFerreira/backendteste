const db = require("../configs/sequelize.js");
const { Op } = require("sequelize");
const utils = require("../utils.js");

const Movie = require("./model.js");

let api = {};

api.findAll = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    let movies = await Movie.findAll();

    if (movies.length > 0) {
      return res.json(movies);
    } else {
      res.set("info", "Ainda nao existem filmes criados");
      return res.status(204).end();
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

api.findById = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.params.id == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um ID" });
    }

    let regexp = new RegExp(/^(0|[1-9][0-9]*)$/);

    if (regexp.test(req.params.id) == false) {
      return res
        .status(400)
        .json({
          info: "Parâmetro inválido. O ID precisa ser um valor numérico",
        });
    }

    let movie = await Movie.findOne({
      where: { id: req.params.id },
    });

    if (movie != null) {
      return res.json(movie);
    } else {
      res.set("info", "Esse filme nao existe");
      return res.status(204).end();
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

api.create = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.body.title == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um título." });
    } else if (req.body.title == "") {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. Título não pode estar vazio." });
    }

    if (req.body.score == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um score" });
    }

    if (req.body.count == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um count" });
    }

    if (req.body.image == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar uma image" });
    }

    let movie = await Movie.create(req.body);

    return res.json(movie);
  } catch (error) {
    return res.status(500).json(error);
  }
};

api.update = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.body.id == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um ID" });
    }

    let regexp = new RegExp(/^(0|[1-9][0-9]*)$/);

    if (regexp.test(req.body.id) == false) {
      return res
        .status(400)
        .json({
          info: "Parâmetro inválido. O ID do filme precisa ser um valor numérico",
        });
    }

    //Verifica se existe esse usuário no banco
    let movie = await Movie.findOne({
      where: { id: req.body.id },
    });

    if (movie != null) {
      let affectedRows = await Movie.update(req.body, {
        where: { id: req.body.id },
      });

      return res.json({
        info: "Filme atualizado com sucesso!",
        affectedRows: affectedRows[0],
      });
    } else {
      return res.json({ info: "Esse filme não existe" });
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

api.delete = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.body.id == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um ID" });
    }

    let regexp = new RegExp(/^(0|[1-9][0-9]*)$/);

    if (regexp.test(req.body.id) == false) {
      return res
        .status(400)
        .json({
          info: "Parâmetro inválido. O ID precisa ser um valor numérico",
        });
    }

    //Verifica se existe esse filme no banco
    let movie = await Movie.findOne({
      where: { id: req.body.id },
    });

    if (movie != null) {
      let affectedRows = await Movie.destroy({
        where: { id: req.body.id },
      });

      return res.json({
        info: "Filme deletado com sucesso!",
        affectedRows: affectedRows[0],
      });
    } else {
      return res.json({ info: "Esse filme não existe" });
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

module.exports = api;
