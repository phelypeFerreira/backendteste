const Usuario = require("../User/model.js");
const utils = require("../utils.js");

const auth = require("../core/auth.js");
const tkn = require("../core/jwt.js");

let api = {};

api.login = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.body.email == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um e-mail." });
    } else if (req.body.email == "") {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. E-mail não pode estar vazio." });
    }

    //Todos os parâmetros estão corretos
    else {
      //Busca o usuário através do email
      let usuario = await Usuario.findOne({
        where: { email: req.body.email },
      });

      //Verifica se existe esse usuário
      if (usuario != null) {
        const token = tkn.createToken({ userId: usuario.id });
        return res.json({
          info: "Autenticação realizada com sucesso!",
          auth: true,
          token: token,
          isAdmin: usuario.isAdmin,
        });
      }
      //Se não existir, retornar mensagem de aviso
      else {
        return res.json({
          info: "Esse e-mail não está cadastrado!",
          auth: false,
        });
      }
    }
  } catch (error) {
    return res.status(500).json({
      info: "Não foi possível realizar o login. Tente novamente",
      error: error,
    });
    console.log(error);
  }
};

api.createUser = async (req, res) => {
  try {
    usuario = await Usuario.create({
      nome: req.body.nome,
      email: req.body.email,
      isAdmin: req.body.isAdmin,
    });

    return res.json(usuario);
  } catch {
    return "ERRO"
  }
};

module.exports = api;
