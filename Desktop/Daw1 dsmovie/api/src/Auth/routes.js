module.exports = (app) => {
  const auth = require("../core/auth.js");
  const loginController = require("./login.js");

  app.post("/login", loginController.login);
  app.post("/create_user", loginController.createUser);
};
