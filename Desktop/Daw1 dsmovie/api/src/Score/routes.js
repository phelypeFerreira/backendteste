module.exports = (app) => {
  const auth = require("../core/auth.js");
  const controller = require("./controller.js");

  //Busca todos os scores
  app.get("/scores", controller.findAll);

  //Busca um score por ID
  app.get("/scores/:id", controller.findById);

  //Atualiza um score por ID
  app.put("/scores", controller.saveScore);

  //Remove um score por ID
  app.delete("/scores", controller.delete);
};
