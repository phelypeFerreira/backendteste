const db = require("../configs/sequelize.js");
const { Op, where } = require("sequelize");
const utils = require("../utils.js");

const Score = require("./model.js");
const Movie = require("../Movie/model.js");
const User = require("../User/model.js");

let api = {};

api.findAll = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    let scores = await Score.findAll();

    if (scores.length > 0) {
      return res.json(scores);
    } else {
      res.set("info", "Ainda nao existem scores criados");
      return res.status(204).end();
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

api.findById = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.params.id == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um ID" });
    }

    let regexp = new RegExp(/^(0|[1-9][0-9]*)$/);

    if (regexp.test(req.params.id) == false) {
      return res.status(400).json({
        info: "Parâmetro inválido. O ID precisa ser um valor numérico",
      });
    }

    let score = await Score.findOne({
      where: { id: req.params.id },
    });

    if (score != null) {
      return res.json(score);
    } else {
      res.set("info", "Esse score nao existe");
      return res.status(204).end();
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

api.saveScore = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.body.value == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um score." });
    }

    if (req.body.user_id != null) {
      let regexp = new RegExp(/^(0|[1-9][0-9]*)$/);

      if (regexp.test(req.body.user_id) == false) {
        return res.status(400).json({
          info: "Parâmetro inválido. O ID do usuario precisa ser um valor numérico.",
        });
      }
    }

    if (req.body.movie_id == null) {
      return res.status(400).json({
        info: "Parâmetro inválido. É necessário informar o id de um filme.",
      });
    }

    //Verifica se existe esse filme no banco
    let movie = await Movie.findOne({
      where: { id: req.body.movie_id },
    });

    //Se existe, atualiza o filme e cria um novo score
    if (movie != null) {
      let scores = 0;

      scores = await Score.findAll({
        where: { movie_id: req.body.movie_id },
      });

      let sum = 0;
      sum = Number(req.body.value) + Number(movie.totalScore);

      let avg = 0;
      avg = sum / (movie.count + 1);

      await Movie.update(
        {
          score: avg,
          count: movie.count + 1,
          totalScore: sum,
        },
        {
          where: { id: req.body.movie_id },
        }
      );

      let affectedRows = await Score.create(req.body);

      return res.json({
        info: "Score atualizado com sucesso!",
        affectedRows: affectedRows[0],
      });
    }
    //Se não existe, retorna mensagem de erro
    else {
      return res.status(400).json({ info: "Esse filme não existe" });
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

api.delete = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.body.id == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um ID" });
    }

    let regexp = new RegExp(/^(0|[1-9][0-9]*)$/);

    if (regexp.test(req.body.id) == false) {
      return res.status(400).json({
        info: "Parâmetro inválido. O ID precisa ser um valor numérico",
      });
    }

    //Verifica se existe esse score no banco
    let score = await Score.findOne({
      where: { id: req.body.id },
    });

    if (score != null) {
      let affectedRows = await Score.destroy({
        where: { id: req.body.id },
      });

      return res.json({
        info: "Score deletado com sucesso!",
        affectedRows: affectedRows[0],
      });
    } else {
      return res.json({ info: "Esse score não existe" });
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

module.exports = api;
