const db = require("../configs/sequelize.js");
const sequelize = db.sequelize;
const { Model, DataTypes } = db.Sequelize;

class Movie extends Model {}

Movie.init(
  {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    value: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    movie_id: {
      //Foreign Key
      type: DataTypes.INTEGER,
      references: {
        model: "tb_movie", // Nome da tabela que o atletaId faz referência
        key: "id", // Nome da coluna dentro da tabela de referência
      },
      allowNull: true,
    },
  },
  { sequelize, modelName: "tb_score", freezeTableName: true, timestamps: false }
);

module.exports = Movie;
