require('dotenv').config()

const jwt        = require('jsonwebtoken')
const tkn        = require('./jwt')
const httpStatus = require('http-status')

const User    = require('../User/controller.js')

const utils      = require('../utils.js');

exports.verifyUser = async (req, res, next) => {
    try {
        const token = utils.getToken(req);

        if (!token) {
            return res.status(401).send({ auth: false, info: 'No token provided.' });
        }

        jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
            if (err) {
                return res.status(httpStatus.UNAUTHORIZED).end("Operação não permitida");
            }
            
            let user = User.findByIdAuth(decoded.userId)

            req.userId = user.id;

            next();
        })

    } catch (err) {
        return Promise.reject('Oops!').catch(err => {
            throw new Error(err);
          });
    }
}
