const db = require("../configs/sequelize.js");
const sequelize = db.sequelize;
const { Model, DataTypes } = db.Sequelize;

class User extends Model {}

User.init(
  {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    nome: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    isAdmin: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    senha: {
      type: DataTypes.STRING,
      allowNull: false,
      //novo
      validate: {
        notEmpty: true,
      },
    },
  },
  { sequelize, modelName: "tb_user", freezeTableName: true, timestamps: false }
);

module.exports = User;
