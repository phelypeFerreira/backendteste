const db = require("../configs/sequelize.js");
const { Op } = require("sequelize");
const utils = require("../utils.js");

const User = require("./model.js");

let api = {};

api.findAll = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    let usuarios = await User.findAll();

    if (usuarios.length > 0) {
      return res.json(usuarios);
    } else {
      res.set("info", "Ainda nao existem usuarios criados");
      return res.status(204).end();
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

api.findById = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.params.id == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um ID" });
    }

    let regexp = new RegExp(/^(0|[1-9][0-9]*)$/);

    if (regexp.test(req.params.id) == false) {
      return res.status(400).json({
        info: "Parâmetro inválido. O ID precisa ser um valor numérico",
      });
    }

    let usuario = await User.findOne({
      where: { id: req.params.id },
    });

    if (usuario != null) {
      return res.json(usuario);
    } else {
      res.set("info", "Esse usuario nao existe");
      return res.status(204).end();
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

api.findByIdAuth = async (userId) => {
  try {
    let usuario = await User.findByPk(userId);
    return usuario;
  } catch (err) {
    return res
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .end("Erro ao buscar usuário por ID");
  }
};

api.findByEmail = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.body.email == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um e-mail" });
    }

    let usuario = await User.findOne({
      where: { email: req.body.email },
    });

    if (usuario != null) {
      return res.json(usuario);
    } else {
      res.set("info", "Esse usuario nao existe");
      return res.status(204).end();
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

api.create = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.body.nome == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um nome." });
    } else if (req.body.nome == "") {
      return res.status(400).json({
        info: "Parâmetro inválido. Nome do usuário não pode estar vazio.",
      });
    }

    if (req.body.email == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um e-mail." });
    } else if (req.body.email == "") {
      return res.status(400).json({
        info: "Parâmetro inválido. E-mail do usuário não pode estar vazio.",
      });
    }

    usuario = await User.create({
      nome: req.body.nome,
      email: req.body.email,
      senha: req.body.senha,
      isAdmin: req.body.isAdmin,
    });

    return res.json(usuario);
  } catch (error) {
    return res.status(500).json(error);
  }
};

api.update = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.body.id == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um ID" });
    }

    let regexp = new RegExp(/^(0|[1-9][0-9]*)$/);

    if (regexp.test(req.body.id) == false) {
      return res.status(400).json({
        info: "Parâmetro inválido. O ID do usuário precisa ser um valor numérico",
      });
    }

    if (req.body.nome != null && req.body.nome == "") {
      return res.status(400).json({
        info: "Parâmetro inválido. Nome do usuário não pode estar vazio.",
      });
    }

    if (req.body.email != null && req.body.email == "") {
      return res.status(400).json({
        info: "Parâmetro inválido. E-mail do usuário não pode estar vazio.",
      });
    }

    //Verifica se existe esse usuário no banco
    let usuario = await User.findOne({
      where: { id: req.body.id },
    });

    if (usuario != null) {
      let affectedRows = await User.update(req.body, {
        where: { id: req.body.id },
      });

      return res.json({
        info: "Usuário atualizado com sucesso!",
        affectedRows: affectedRows[0],
      });
    } else {
      return res.json({ info: "Esse usuário não existe" });
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

api.delete = async (req, res) => {
  try {
    utils.showRequestUrl(req);

    if (req.body.id == null) {
      return res
        .status(400)
        .json({ info: "Parâmetro inválido. É necessário informar um ID" });
    }

    let regexp = new RegExp(/^(0|[1-9][0-9]*)$/);

    if (regexp.test(req.body.id) == false) {
      return res.status(400).json({
        info: "Parâmetro inválido. O ID precisa ser um valor numérico",
      });
    }

    //Verifica se existe esse usuário no banco
    let usuario = await User.findOne({
      where: { id: req.body.id },
    });

    if (usuario != null) {
      let affectedRows = await User.destroy({
        where: { id: req.body.id },
      });

      return res.json({
        info: "Usuário deletado com sucesso!",
        affectedRows: affectedRows[0],
      });
    } else {
      return res.json({ info: "Esse usuário não existe" });
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

module.exports = api;
