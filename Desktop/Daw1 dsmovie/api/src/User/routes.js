module.exports = (app) => {
  const auth = require("../core/auth.js");
  const controller = require("./controller.js");

  //Busca todos os usuarios
  app.get("/users", controller.findAll);

  //Busca um usuario por ID
  app.get("/users/:id", controller.findById);

  //Busca um usuario pelo email
  app.post("/users/:id", controller.findByEmail);

  //Criar um novo usuario
  app.post("/users", controller.create);

  //Atualiza um usuario por ID
  app.put("/users", controller.update);

  //Remove um usuario por ID
  app.delete("/users", controller.delete);
};
