import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Listing from "pages/Listing";
import Form from "pages/Form";
import Navbar from "components/Navbar";
import Login from "../src/pages/Login";
import Register from "../src/pages/Register";
import EditMovies from "../src/pages/EditMovies";
import CreateMovie from "../src/pages/CreateMovie";

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<Listing />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/form">
          <Route path=":movieId" element={<Form />} />
        </Route>
        <Route path="/edit-movies">
          <Route path=":movieId" element={<EditMovies />} />
        </Route>
        <Route path="/create-movies" element={<CreateMovie />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
