import FormEditMovies from "components/FormEditMovies";
import { useParams } from "react-router-dom";

function Form() {
  const params = useParams();

  return <FormEditMovies movieId={`${params.movieId}`} />;
}

export default Form;
