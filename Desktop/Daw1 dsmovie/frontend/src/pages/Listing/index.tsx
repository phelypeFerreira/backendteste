import axios from "axios";
import { useState, useEffect } from "react";
import MovieCard from "components/MovieCard";
import Pagination from "components/Pagination";
import { BASE_URL } from "utils/requests";
import { MoviePage } from "types/movie";
import ButtonCreateMovie from "../../components/ButtonCreateMovie";

function Listing() {
  const isAdmin = localStorage.getItem("admin");
  const [pageNumber, setPageNumber] = useState(0);

  const [page] = useState<MoviePage>({
    content: [],
    last: true,
    totalPages: 0,
    totalElements: 0,
    size: 12,
    number: 0,
    first: true,
    numberOfElements: 0,
    empty: true,
  });
  const [movies, setMovies] = useState<any>([]);

  useEffect(() => {
    axios
      .get(`${BASE_URL}/movies?size=12&page=${pageNumber}&sort=title`)
      .then((response) => {
        const data = response.data as MoviePage;
        setMovies(data);
      })
      .catch((err) => alert(err));
  }, [pageNumber]);

  const handPageChange = (newPageNumber: number) => {
    setPageNumber(newPageNumber);
  };

  return (
    <>
      {isAdmin === "true" && <ButtonCreateMovie />}
      <Pagination page={page} onChange={handPageChange} />

      <div className="container">
        <div className="row">
          {movies.map(
            (movie: {
              id: number;
              title: string;
              score: number;
              image: string;
              count: number;
            }) => (
              <div key={movie.id} className="col-sm-6 col-lg-4 col-xl-3 mb-3">
                <MovieCard movie={movie} />
              </div>
            )
          )}
        </div>
      </div>
    </>
  );
}

export default Listing;
