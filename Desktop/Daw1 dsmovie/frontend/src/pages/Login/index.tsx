import { Link } from "react-router-dom";
import { useState } from "react";
import axios from "axios";
import { BASE_URL } from "utils/requests";
// import movieIMG from "../../assets/movie.svg";
import { LayoutComponents } from "../../components/LayoutComponents";
import { useNavigate } from "react-router-dom";
import "./styles.css";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  let navigate = useNavigate();

  function handleLogin() {
    axios
      .post(`${BASE_URL}/login`, {
        email: email,
        password: password,
      })
      .then((response) => {
        localStorage.setItem("token", response.data.token);
        localStorage.setItem("email", email);
        localStorage.setItem("admin", response.data.isAdmin);
        navigate("/");
      })
      .catch((err) => alert(err));
  }

  return (
    <LayoutComponents className="body">
      {/* <form className="login-form"> */}
      
      <span className="login-form-title"> Bem vindo ao Dsmovie</span>

      <span className="login-form-title">
        {/* <img src={movieIMG} alt="Jovem Programador" /> */}
      </span>

      <div className="wrap-input">
        <input
          className={email !== "" ? "has-val input" : "input"}
          type="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <span className="focus-input" data-placeholder="Email"></span>
      </div>

      <div className="wrap-input">
        <input
          className={password !== "" ? "has-val input" : "input"}
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <span className="focus-input" data-placeholder="Password"></span>
      </div>

      <div className="container-login-form-btn" onClick={handleLogin}>
        <button className="login-form-btn">Login</button>
      </div>

      <div className="text-center">
        <span className="txt1">Não possui conta? </span>
        <Link className="txt2" to="/register">
          Criar conta.
        </Link>
      </div>
      {/* </form> */}
    </LayoutComponents>
  );
};

export default Login;
