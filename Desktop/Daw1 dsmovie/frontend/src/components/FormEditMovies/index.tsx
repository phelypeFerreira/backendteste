import axios, { AxiosRequestConfig } from "axios";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Movie } from "types/movie";
import { BASE_URL } from "utils/requests";
import { validateEmail } from "utils/validate";
import "./styles.css";

type Props = {
  movieId: string;
};
function EditMovies({ movieId }: Props) {
  const navigate = useNavigate();

  const [movie, setMovie] = useState<Movie>();
  const [title, setTitle] = useState("");
  const [image, setImage] = useState("");

  useEffect(() => {
    axios.get(`${BASE_URL}/movies/${movieId}`).then((Response) => {
      setMovie(Response.data);
    });
  }, [movieId]);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    //para nao recarregar
    event.preventDefault();

    // const title = (event.target as any).title.value;
    // const image = (event.target as any).image.value;
    axios
      .put(`${BASE_URL}/movies`, {
        id: movieId,
        title: title,
        image: image,
      })
      .then(() => {
        navigate("/");
      });
  };

  useEffect(() => {
    axios.get(`${BASE_URL}/movies/${movieId}`).then((response) => {
      setMovie(response.data);
      setTitle(response.data.title);
      setImage(response.data.image);
    });
  }, [movieId]);

  function handleDeleteMovie() {
    axios
      .delete(`${BASE_URL}/movies`, {
        data: {
          id: movieId,
        },
      })
      .then(() => {
        navigate("/");
      });
  }

  return (
    <div className="dsmovie-form-container">
      <img
        className="dsmovie-movie-card-image"
        src={movie?.image}
        alt={movie?.title}
      />
      <div className="dsmovie-card-bottom-container">
        <h3>{movie?.title}</h3>
        <form className="dsmovie-form" onSubmit={handleSubmit}>
          <div className="form-group dsmovie-form-group">
            <label htmlFor="title">Título:</label>
            <input
              value={title}
              onChange={(event) => setTitle(event.target.value)}
              className="dsmovie-input"
            />
          </div>
          <div className="form-group dsmovie-form-group">
            <label htmlFor="image">Imagem:</label>
            <input
              value={image}
              onChange={(event) => setImage(event.target.value)}
              className="dsmovie-input"
            />
          </div>
          <div className="dsmovie-form-btn-container">
            <button type="submit" className="btn btn-primary dsmovie-btn">
              Salvar
            </button>
          </div>
        </form>
        <Link to="/">
          <button className="btn btn-primary dsmovie-btn mt-3">Cancelar</button>
        </Link>
        <button
          className="btn btn-primary dsmovie-btn mt-3"
          onClick={handleDeleteMovie}
        >
          Excluir
        </button>
      </div>
    </div>
  );
}

export default EditMovies;
