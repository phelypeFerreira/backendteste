import { Link } from "react-router-dom";
import "./styles.css";

function ButtonCreateMovie() {
  return (
    <Link className="button_container" to="/create-movies">
      <div className="button">
        <p>Criar filme</p>
      </div>
    </Link>
  );
}

export default ButtonCreateMovie;
