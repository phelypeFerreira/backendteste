import axios from "axios";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { BASE_URL } from "utils/requests";
import "./styles.css";

function FormCreateMovie() {
  const navigate = useNavigate();

  const [title, setTitle] = useState("");
  const [image, setImage] = useState("");

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    //para nao recarregar
    event.preventDefault();

    axios
      .post(`${BASE_URL}/movies`, {
        title: title,
        score: 0,
        count: 0,
        totalScore: 0,
        image: image,
      })
      .then(() => {
        navigate("/");
      })
      .catch((err) => alert(err));
  };

  return (
    <div className="dsmovie-form-container">
      <div className="dsmovie-card-bottom-container">
        <form className="dsmovie-form" onSubmit={handleSubmit}>
          <div className="form-group dsmovie-form-group">
            <div className="form-group dsmovie-form-group">
              <label htmlFor="title">Título:</label>
              <input
                value={title}
                onChange={(event) => setTitle(event.target.value)}
                className="dsmovie-input"
              />
            </div>
            <div className="form-group dsmovie-form-group">
              <label htmlFor="title">Imagem:</label>
              <input
                value={image}
                onChange={(event) => setImage(event.target.value)}
                className="dsmovie-input"
              />
            </div>
          </div>
          <div className="dsmovie-form-btn-container">
            <button type="submit" className="btn btn-primary dsmovie-btn">
              Criar
            </button>
          </div>
        </form>
        <Link to="/">
          <button className="btn btn-primary dsmovie-btn mt-3">Cancelar</button>
        </Link>
      </div>
    </div>
  );
}

export default FormCreateMovie;
